## Votação Service

Este serviço é responsável por fazer o gerenciamento das sessões de votação. A abertura e fechamento das pautas fica por conta do pauta-service

## Stack Utilizada

* Serviços
    * Spring WebFlux
    * SpringData Mongo Reactive
    * JUnit Jupiter
    * Stella (Validação de CPF)
    * Springfox Swagger
    * Lombok
* Infraestrutura
    * Kafka
    * MongoDB
    * Docker
    * Docker-compose
    
## Explicação do fluxo

```
1. Ao receber uma requisição, o serviço de pauta vai abrir uma pauta e publicar a abertura no tópico pauta.aberta.v1
  1.1.  Caso o serviço de votação esteja fora, a sessão ainda será aberta no momento em que ele voltar pois a mensagem será consumida e o processo seguirá normalmente
2. Serviço de votacao vai escutar a abertura de uma pauta e dar inicio a sessão de votos
  2.1. A sessão não permitirá votos depois do "horarioFechamento" informado no tópico de abertura
  2.2. A sessão continuará sem apuramento até que receba a mensagem de pauta finalizada, enviada pelo serviço de pautas
3. Assim que acabar o prazo, o serviço de pautas vai mudar a situação para FECHADO e publicar no tópico pauta.finalizada.v1 para que seja feita a apuração dos votos
  3.1. Caso o serviço de votação esteja fora, a pauta permanecerá como FECHADO
4. O serviço de votação vai ler o tópico de pauta finalizada e fará a apuração dos votos e publicar o resultado no topico resultado.votacao.v1
  4.1. Caso o serviço de pautas esteja fora, a pauta será atualizada quando ele subir novamente
5. O serviço de pautas vai ler o tópico de resultado da votação e vai atualizar a pauta para APURADO junto com o resultado e resumo da votação
6. O resultado pode ser consultado no endpoint de GET de pauta usando o UUID da pauta ou consumido do tópico resultado.votacao.v1
```

## Explicação da estrutura

- **Serparação em 2 serviços**: Dessa forma, é possível escalar apenas a aplicação de votação caso haja um trafego muito grande de votos sem gastar recurso com a API de pautas
- **Comunicação entre os serviços via mensageria**: Dessa forma, existe um desacoplamento grande entre os 2 sistemas, permitindo que o processo de votação, apuração e divulgação dos resultados da votação ocorre mesmo com o serviço de pautas fora do ar

## Passos para subir o ambiente

### Subir o ambiente completo (subirá o pauta-service na porta 9010)

1. Fazer o build da aplicação
> ./gradlew clean build
2. Subir as dependencias:
> docker-compose -f ambiente-completo.yml up -d
3. Subir a aplicação
>./gradlew bootRun
4. Acessar a documentação:
> http://localhost:9011/swagger-ui/index.html

### Subir o ambiente individual (o pauta-service precisará ser executado separadamente)

1. Fazer o build da aplicação
> ./gradlew clean build
2. Subir as dependencias:
> docker-compose -f ambiente-individual.yml up -d
3. Subir a aplicação
>./gradlew bootRun
4. Subir o pauta-service
-  Acessar a pasta do projeto e rodar o comando:
> ./gradlew bootRun
5. Acessar a documentação:
> http://localhost:9010/swagger-ui/index.html

## Exemplos de request

### Votar

Método | URL
-------|-------
POST   | http://localhost:9010/v1/votos

Body:

```json
{
  "cpf": "49250248091",
  "opcaoVoto": "SIM",
  "pauta": "618da860a50b987300341beb"
}
```

Response:
```text
618d5fc15a15e43ddd9fdb26
```
