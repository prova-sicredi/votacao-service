FROM openjdk:11
ARG jarFile
ENV MONGO_HOST="localhost:27018" KAFKA_BROKER="localhost:9092" SERVER_PORT="9011"
RUN mkdir /app
COPY ${jarFile} /app/votacao-service.jar
WORKDIR /app
EXPOSE 9011
ENTRYPOINT ["java", "-jar", "votacao-service.jar", "--spring.data.mongodb.host=${MONGO_HOST}", "--votacao.cloud.kafka.binder.brokers=${KAFKA_BROKER}", "--server.port=${SERVER_PORT}"]