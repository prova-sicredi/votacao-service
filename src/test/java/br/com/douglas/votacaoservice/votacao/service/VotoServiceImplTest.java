package br.com.douglas.votacaoservice.votacao.service;

import br.com.douglas.votacaoservice.app.exception.GenericExceptionBuilder;
import br.com.douglas.votacaoservice.app.exception.model.GenericException;
import br.com.douglas.votacaoservice.fixture.Fixture;
import br.com.douglas.votacaoservice.sessao.service.SessaoVotacaoService;
import br.com.douglas.votacaoservice.usuario.UsuarioRestClient;
import br.com.douglas.votacaoservice.votacao.mapper.VotoMapper;
import br.com.douglas.votacaoservice.votacao.repository.VotoRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static br.com.douglas.votacaoservice.fixture.Fixture.CPF;
import static br.com.douglas.votacaoservice.fixture.GenericExceptionFixture.genericException;
import static br.com.douglas.votacaoservice.fixture.UsuarioFixture.usuario;
import static br.com.douglas.votacaoservice.fixture.VotoFixture.voto;
import static br.com.douglas.votacaoservice.fixture.VotoRequestFixture.votoRequest;
import static br.com.douglas.votacaoservice.usuario.enumeration.SituacaoUsuario.ABLE_TO_VOTE;
import static br.com.douglas.votacaoservice.usuario.enumeration.SituacaoUsuario.UNABLE_TO_VOTE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

@ExtendWith(MockitoExtension.class)
class VotoServiceImplTest {

    @Mock
    private UsuarioRestClient usuarioRestClient;

    @Mock
    private GenericExceptionBuilder genericExceptionBuilder;

    @Mock
    private SessaoVotacaoService sessaoVotacaoService;

    @Mock
    private VotoMapper votoMapper;

    @Mock
    private VotoRepository votoRepository;

    @InjectMocks
    private VotoServiceImpl votoService;

    @DisplayName("Dado um VotoRequest, caso o CPF esteja apto a votar, deve salvar" +
            "o voto corretamente")
    @Test
    void deveSalvarOVotoCorretamente(){
        doReturn(Mono.just(usuario(ABLE_TO_VOTE))).when(usuarioRestClient).consultarSituacaoUsuario(CPF);
        doReturn(Mono.just(voto())).when(votoMapper).toVoto(votoRequest());
        doReturn(Mono.just(voto())).when(votoRepository).salvar(voto());
        doReturn(Mono.just(voto())).when(sessaoVotacaoService).adicionarVotoASessao(voto());
        doReturn(new GenericException()).when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(votoService.registrarVoto(votoRequest()))
                .expectNext(Fixture.UUID_VOTO)
                .verifyComplete();
        verify(votoMapper, times(1)).toVoto(votoRequest());
        verify(votoRepository, times(1)).salvar(voto());
        verify(sessaoVotacaoService, timeout(1)).adicionarVotoASessao(voto());
    }

    @DisplayName("Dado um VotoRequest, caso o CPF nao esteja apto a votar, deve retornar um erro")
    @Test
    void deveRetornarErroParaUsuarioInapto(){
        String mensagem = "Erro";
        String mensagemUsuairo = "Erro usuario";
        doReturn(Mono.just(usuario(UNABLE_TO_VOTE))).when(usuarioRestClient).consultarSituacaoUsuario(CPF);
        doReturn(genericException(UNPROCESSABLE_ENTITY, mensagem, mensagemUsuairo))
                .when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        doReturn(Mono.just(voto())).when(votoMapper).toVoto(votoRequest());
        StepVerifier.create(votoService.registrarVoto(votoRequest()))
                .expectErrorMatches(exception -> exception instanceof GenericException
                        && exception.getMessage().equals(mensagem)
                        && ((GenericException) exception).getResponseError().getStatus().equals(UNPROCESSABLE_ENTITY))
                .verify();
        verify(votoRepository, never()).salvar(any());
        verify(sessaoVotacaoService, never()).adicionarVotoASessao(any());
    }

}