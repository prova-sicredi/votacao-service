package br.com.douglas.votacaoservice.votacao.mapper;

import br.com.douglas.votacaoservice.app.provider.DateTimeProvider;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.test.StepVerifier;

import static br.com.douglas.votacaoservice.fixture.Fixture._11_11_2021_22_15_00;
import static br.com.douglas.votacaoservice.fixture.VotoFixture.votoNaoSalvo;
import static br.com.douglas.votacaoservice.fixture.VotoRequestFixture.votoRequest;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class VotoMapperTest {

    @Mock
    private DateTimeProvider dateTimeProvider;

    @InjectMocks
    private VotoMapper votoMapper;

    @DisplayName("Dado um VotoRequest, deve mapear corretamente para voto")
    @Test
    void deveMapearCorretamente() {
        doReturn(_11_11_2021_22_15_00).when(dateTimeProvider).now();
        StepVerifier.create(votoMapper.toVoto(votoRequest()))
                .expectNext(votoNaoSalvo())
                .verifyComplete();
    }

}