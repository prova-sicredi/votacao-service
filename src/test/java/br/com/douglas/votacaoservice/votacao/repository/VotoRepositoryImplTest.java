package br.com.douglas.votacaoservice.votacao.repository;

import br.com.douglas.votacaoservice.app.exception.GenericExceptionBuilder;
import br.com.douglas.votacaoservice.app.exception.model.GenericException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static br.com.douglas.votacaoservice.fixture.GenericExceptionFixture.genericException;
import static br.com.douglas.votacaoservice.fixture.VotoEntityFixture.votoEntity;
import static br.com.douglas.votacaoservice.fixture.VotoFixture.voto;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class VotoRepositoryImplTest {

    @Mock
    private VotoMongoRepository votoMongoRepository;

    @Mock
    private GenericExceptionBuilder genericExceptionBuilder;

    @InjectMocks
    private VotoRepositoryImpl votoRepository;

    @DisplayName("Dado um Voto, deve chamar o save e retornar o voto corretamente")
    @Test
    void deveSalvarOVoto() {
        doReturn(Mono.just(votoEntity())).when(votoMongoRepository).save(votoEntity());
        doReturn(new GenericException()).when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(votoRepository.salvar(voto()))
                .expectNext(voto())
                .verifyComplete();
        verify(votoMongoRepository, times(1)).save(votoEntity());
    }

    @DisplayName("Dado que nao seja possivel salvar um voto, deve retornar uma exception")
    @Test
    void deveRetornarUmaException() {
        String mensagemErro = "Ocorreu um erro ao salvar o voto no banco";
        String mensagemUsuario = "Tente novamente dentro de alguns minutos.";
        doReturn(Mono.empty()).when(votoMongoRepository).save(votoEntity());
        doReturn(genericException(HttpStatus.BAD_GATEWAY, mensagemErro, mensagemUsuario))
                .when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(votoRepository.salvar(voto()))
                .expectErrorMatches(exception -> exception instanceof GenericException
                        && exception.getMessage().equals(mensagemErro)
                        && ((GenericException) exception).getResponseError().getStatus().equals(HttpStatus.BAD_GATEWAY))
                .verify();
    }

}