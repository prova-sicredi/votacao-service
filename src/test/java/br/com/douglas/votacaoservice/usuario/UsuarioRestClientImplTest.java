package br.com.douglas.votacaoservice.usuario;

import br.com.douglas.votacaoservice.app.exception.GenericExceptionBuilder;
import br.com.douglas.votacaoservice.app.exception.model.GenericException;
import br.com.douglas.votacaoservice.fixture.Fixture;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.test.StepVerifier;

import java.io.IOException;

import static br.com.douglas.votacaoservice.fixture.GenericExceptionFixture.genericException;
import static br.com.douglas.votacaoservice.fixture.UsuarioFixture.usuario;
import static br.com.douglas.votacaoservice.usuario.enumeration.SituacaoUsuario.ABLE_TO_VOTE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.springframework.http.HttpStatus.BAD_GATEWAY;
import static org.springframework.http.HttpStatus.NOT_FOUND;

class UsuarioRestClientImplTest {

    private final WebClient webClient = WebClient.create("http://localhost:"+mockWebServer.getPort());
    private final GenericExceptionBuilder genericExceptionBuilder = mock(GenericExceptionBuilder.class);

    private final UsuarioRestClientImpl usuarioRestClient = new UsuarioRestClientImpl(webClient, genericExceptionBuilder);

    private static MockWebServer mockWebServer;

    @BeforeAll
    static void setup() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @AfterAll
    static void cleanup() throws IOException {
        mockWebServer.shutdown();
    }

    @DisplayName("Dado um CPF e que o serviço de usuario retorne ABLE_TO_VOTE, deve retornar o usuario corretamente")
    @Test
    void deveRetornarOUsuarioCorretamente() throws JsonProcessingException {
        mockWebServer.enqueue(new MockResponse()
                .setBody(new ObjectMapper().writeValueAsString(usuario(ABLE_TO_VOTE)))
                .addHeader("Content-type", "application/json"));
        doReturn(new GenericException()).when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(usuarioRestClient.consultarSituacaoUsuario(Fixture.CPF))
                .expectNext(usuario(ABLE_TO_VOTE))
                .verifyComplete();
    }

    @DisplayName("Dado um CPF e que o serviço de retorne um erro 5xx, deve retornar uma exception")
    @Test
    void deveRetornarErroComStatusCorretoParaErros500() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(BAD_GATEWAY.value()));
        String mensagem = "Ocorreu erro ao tentar validar a situação do usuário";
        String mensagemUsuario = "Tente novamente dentro de alguns minutos.";
        doReturn(genericException(BAD_GATEWAY, mensagem, mensagemUsuario))
                .when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(usuarioRestClient.consultarSituacaoUsuario(Fixture.CPF))
                .expectErrorMatches(exception -> exception instanceof GenericException
                        && exception.getMessage().equals(mensagem)
                        && ((GenericException) exception).getResponseError().getStatus().equals(BAD_GATEWAY))
                .verify();
    }

    @DisplayName("Dado um CPF e que o serviço de retorne um erro 4xx, deve retornar uma exception")
    @Test
    void deveRetornarErroComStatusCorretoParaErros400() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(NOT_FOUND.value()));
        String mensagem = "Ocorreu erro ao tentar validar a situação do usuário";
        String mensagemUsuario = "Tente novamente dentro de alguns minutos.";
        doReturn(genericException(NOT_FOUND, mensagem, mensagemUsuario))
                .when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(usuarioRestClient.consultarSituacaoUsuario(Fixture.CPF))
                .expectErrorMatches(exception -> exception instanceof GenericException
                        && exception.getMessage().equals(mensagem)
                        && ((GenericException) exception).getResponseError().getStatus().equals(NOT_FOUND))
                .verify();
    }

}