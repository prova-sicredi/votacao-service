package br.com.douglas.votacaoservice.sessao.evento.handler;

import br.com.douglas.votacaoservice.fixture.SessaoVotacaoAbertaFixture;
import br.com.douglas.votacaoservice.fixture.SessaoVotacaoFixture;
import br.com.douglas.votacaoservice.sessao.mapper.SessaoVotacaoMapper;
import br.com.douglas.votacaoservice.sessao.model.SessaoVotacao;
import br.com.douglas.votacaoservice.sessao.service.SessaoVotacaoService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static br.com.douglas.votacaoservice.fixture.SessaoVotacaoAbertaFixture.sessaoVotacaoAberta;
import static br.com.douglas.votacaoservice.fixture.SessaoVotacaoFixture.sessaoVotacao;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SessaoVotacaoAbertaHandlerImplTest {

    @Mock
    private SessaoVotacaoService sessaoVotacaoService;

    @Mock
    private SessaoVotacaoMapper sessaoVotacaoMapper;

    @InjectMocks
    private SessaoVotacaoAbertaHandlerImpl sessaoVotacaoAbertaHandler;

    @DisplayName("Dado uma SessaoVotacaoAberta, deve chamar o mapper e a service com os parametros corretos")
    @Test
    void deveChamarAbrirSessao() {
        doReturn(Mono.just(sessaoVotacao())).when(sessaoVotacaoMapper).toSessaoVotacao(sessaoVotacaoAberta());
        doReturn(Mono.just(sessaoVotacao())).when(sessaoVotacaoService).abrirSessao(sessaoVotacao());
        StepVerifier.create(sessaoVotacaoAbertaHandler.abrirSessaoVotacao(sessaoVotacaoAberta()))
                .expectNext(sessaoVotacao())
                .verifyComplete();
        verify(sessaoVotacaoMapper, times(1)).toSessaoVotacao(sessaoVotacaoAberta());
        verify(sessaoVotacaoService, times(1)).abrirSessao(sessaoVotacao());
    }

}