package br.com.douglas.votacaoservice.sessao.repository;

import br.com.douglas.votacaoservice.app.exception.GenericExceptionBuilder;
import br.com.douglas.votacaoservice.app.exception.model.GenericException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static br.com.douglas.votacaoservice.fixture.Fixture.UUID_PAUTA;
import static br.com.douglas.votacaoservice.fixture.GenericExceptionFixture.genericException;
import static br.com.douglas.votacaoservice.fixture.SessaoVotacaoEntityFixture.sessaoVotacaoEntity;
import static br.com.douglas.votacaoservice.fixture.SessaoVotacaoFixture.sessaoVotacao;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.BAD_GATEWAY;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ExtendWith(MockitoExtension.class)
class SessaoVotacaoRepositoryImplTest {

    @Mock
    private SessaoVotacaoMongoRepository sessaoVotacaoMongoRepository;

    @Mock
    private GenericExceptionBuilder genericExceptionBuilder;

    @InjectMocks
    private SessaoVotacaoRepositoryImpl sessaoVotacaoRepository;

    @DisplayName("Dado uma SessaoVotacao, deve salvar corretamente")
    @Test
    void deveSalvarUmaSessao() {
        doReturn(Mono.just(sessaoVotacaoEntity())).when(sessaoVotacaoMongoRepository).save(sessaoVotacaoEntity());
        doReturn(new GenericException()).when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(sessaoVotacaoRepository.salvar(sessaoVotacao()))
                .expectNext(sessaoVotacao())
                .verifyComplete();
        verify(sessaoVotacaoMongoRepository, times(1)).save(sessaoVotacaoEntity());
    }

    @DisplayName("Dado uma SessaoVotacao e que salvar retorne um Mono vazio, deve lançar uma exception")
    @Test
    void deveRetornarErroAoSalvarUmaSesao() {
        String mensagemUsuario = "Tente novamente mais tarde";
        String mensagem = "Erro ao salvar a sessão para a pauta " + UUID_PAUTA;
        doReturn(Mono.empty()).when(sessaoVotacaoMongoRepository).save(sessaoVotacaoEntity());
        doReturn(genericException(BAD_GATEWAY, mensagem, mensagemUsuario))
                .when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(sessaoVotacaoRepository.salvar(sessaoVotacao()))
                .expectErrorMatches(exception -> exception instanceof GenericException
                        && exception.getMessage().equals(mensagem)
                        && ((GenericException) exception).getResponseError().getStatus().equals(BAD_GATEWAY))
                .verify();
        verify(sessaoVotacaoMongoRepository, times(1)).save(sessaoVotacaoEntity());
    }

    @DisplayName("Dado um UUID para uma pauta, deve retornar a sessao correspondente")
    @Test
    void deveRetornarUmaSessao() {
        doReturn(Mono.just(sessaoVotacaoEntity())).when(sessaoVotacaoMongoRepository).findByUuidPauta(UUID_PAUTA);
        doReturn(new GenericException()).when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(sessaoVotacaoRepository.buscarPorUuidPauta(UUID_PAUTA))
                .expectNext(sessaoVotacao())
                .verifyComplete();
        verify(sessaoVotacaoMongoRepository, times(1)).findByUuidPauta(UUID_PAUTA);
    }

    @DisplayName("Dado um UUID para uma pauta, caso nao exista uma sessao, deve retornar um erro")
    @Test
    void deveRetornarErroAoBuscarSessao() {
        String mensagemUsuario = "Sessão de votação não encontrada para a pauta informada.";
        String mensagem = "Não existe uma sessão para a pauta " + UUID_PAUTA;
        doReturn(Mono.empty()).when(sessaoVotacaoMongoRepository).save(sessaoVotacaoEntity());
        doReturn(genericException(NOT_FOUND, mensagem, mensagemUsuario))
                .when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(sessaoVotacaoRepository.salvar(sessaoVotacao()))
                .expectErrorMatches(exception -> exception instanceof GenericException
                        && exception.getMessage().equals(mensagem)
                        && ((GenericException) exception).getResponseError().getStatus().equals(NOT_FOUND))
                .verify();
        verify(sessaoVotacaoMongoRepository, times(1)).save(sessaoVotacaoEntity());
    }

}