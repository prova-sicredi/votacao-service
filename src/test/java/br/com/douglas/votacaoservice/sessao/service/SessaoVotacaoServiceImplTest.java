package br.com.douglas.votacaoservice.sessao.service;

import br.com.douglas.votacaoservice.app.exception.GenericExceptionBuilder;
import br.com.douglas.votacaoservice.app.exception.model.GenericException;
import br.com.douglas.votacaoservice.app.provider.DateTimeProvider;
import br.com.douglas.votacaoservice.sessao.repository.SessaoVotacaoRepository;
import br.com.douglas.votacaoservice.votacao.enumeration.OpcaoVoto;
import org.assertj.core.util.Maps;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.HashMap;
import java.util.Map;

import static br.com.douglas.votacaoservice.fixture.Fixture.*;
import static br.com.douglas.votacaoservice.fixture.GenericExceptionFixture.genericException;
import static br.com.douglas.votacaoservice.fixture.SessaoVotacaoFixture.sessaoVotacao;
import static br.com.douglas.votacaoservice.fixture.VotoFixture.voto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.PRECONDITION_FAILED;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

@ExtendWith(MockitoExtension.class)
class SessaoVotacaoServiceImplTest {

    @Mock
    private SessaoVotacaoRepository sessaoVotacaoRepository;

    @Mock
    private DateTimeProvider dateTimeProvider;

    @Mock
    private GenericExceptionBuilder genericExceptionBuilder;

    @InjectMocks
    private SessaoVotacaoServiceImpl sessaoVotacaoService;

    @DisplayName("Dada uma SessaoVotacao, deve salvar a sessao corretamente no repository")
    @Test
    void deveSalvarASessaoAberta() {
        doReturn(Mono.just(sessaoVotacao())).when(sessaoVotacaoRepository).salvar(sessaoVotacao());
        StepVerifier.create(sessaoVotacaoService.abrirSessao(sessaoVotacao()))
                .expectNext(sessaoVotacao())
                .verifyComplete();
        verify(sessaoVotacaoRepository, times(1)).salvar(sessaoVotacao());
    }

    @DisplayName("Dado um Voto, deve  buscar a pauta correspondente," +
            " adicionar esse voto corretamente no mapa de votos da sessao e salvar a sessao")
    @Test
    void deveAdicionarVotosASessao() {
        doReturn(Mono.just(sessaoVotacao(new HashMap<>()))).when(sessaoVotacaoRepository).buscarPorUuidPauta(UUID_PAUTA);
        doReturn(_11_11_2021_22_15_00).when(dateTimeProvider).now();
        doReturn(new GenericException()).when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        doReturn(Mono.just(sessaoVotacao(VOTOS))).when(sessaoVotacaoRepository).salvar(sessaoVotacao(VOTOS));
        StepVerifier.create(sessaoVotacaoService.adicionarVotoASessao(voto()))
                .expectNext(voto())
                .verifyComplete();
        verify(sessaoVotacaoRepository, times(1)).salvar(sessaoVotacao(VOTOS));
        verify(sessaoVotacaoRepository, times(1)).buscarPorUuidPauta(UUID_PAUTA);
    }

    @DisplayName("Dado um voto, caso a sessao já esteja fechada, deve retornar um erro e nao chamar o metodo de salvar")
    @Test
    void deveRetornarErroDeSessaoFechada() {
        String mensagem = String.format("A sessão da pauta %s já fechou.", UUID_PAUTA);
        String mensagemUsuario = "Você não pode mais votar nesta pauta. Tente votar na próxima.";
        doReturn(Mono.just(sessaoVotacao(new HashMap<>()))).when(sessaoVotacaoRepository).buscarPorUuidPauta(UUID_PAUTA);
        doReturn(_11_11_2021_22_23_00).when(dateTimeProvider).now();
        doReturn(genericException(UNPROCESSABLE_ENTITY, mensagem, mensagemUsuario))
                .when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(sessaoVotacaoService.adicionarVotoASessao(voto()))
                .expectErrorMatches(exception -> exception instanceof GenericException
                        && exception.getMessage().equals(mensagem)
                        && ((GenericException) exception).getResponseError().getStatus().equals(UNPROCESSABLE_ENTITY))
                .verify();
        verify(sessaoVotacaoRepository, never()).salvar(sessaoVotacao(VOTOS));
        verify(sessaoVotacaoRepository, times(1)).buscarPorUuidPauta(UUID_PAUTA);
    }

    @DisplayName("Dado um voto, caso a já exista um voto para o CPF na sessao, deve retornar um erro " +
            "nao chamar o metodo salvar")
    @Test
    void deveRetornarErroDeVotoUnico() {
        String mensagem = String.format("Já consta um voto para o CPF %s.", CPF);
        String mensagemUsuario = "Cada usuário só pode votar uma vez.";
        doReturn(Mono.just(sessaoVotacao(VOTOS))).when(sessaoVotacaoRepository).buscarPorUuidPauta(UUID_PAUTA);
        doReturn(_11_11_2021_22_23_00).when(dateTimeProvider).now();
        doReturn(genericException(PRECONDITION_FAILED, mensagem, mensagemUsuario))
                .when(genericExceptionBuilder).buildGenericException(any(), any(), any());
        StepVerifier.create(sessaoVotacaoService.adicionarVotoASessao(voto()))
                .expectErrorMatches(exception -> exception instanceof GenericException
                        && exception.getMessage().equals(mensagem)
                        && ((GenericException) exception).getResponseError().getStatus().equals(PRECONDITION_FAILED))
                .verify();
        verify(sessaoVotacaoRepository, never()).salvar(sessaoVotacao(VOTOS));
        verify(sessaoVotacaoRepository, times(1)).buscarPorUuidPauta(UUID_PAUTA);
    }

    @DisplayName("Dados um voto, deve contabilizar os votos gerando o resultado e resumo corretos")
    @ParameterizedTest
    @CsvSource(value = {"SIM,SIM,SIM: 1 | NAO: 0", "NAO,NAO,NAO: 1 | SIM: 0", "Empate,Empate,SIM: 0 | NAO: 0"})
    void deveFecharASessaoComOsVotosContados(String voto, String resultado, String resumo) {
        Map<String, OpcaoVoto> votos = new HashMap<>();
        if (!voto.equals("Empate")) {
            votos = Maps.newHashMap(CPF, OpcaoVoto.valueOf(voto));
        }
        doReturn(Mono.just(sessaoVotacao(votos))).when(sessaoVotacaoRepository).buscarPorUuidPauta(UUID_PAUTA);
        doReturn(Mono.just(sessaoVotacao(votos, resultado, resumo)))
                .when(sessaoVotacaoRepository).salvar(sessaoVotacao(votos, resultado, resumo));
        StepVerifier.create(sessaoVotacaoService.fecharSessao(UUID_PAUTA))
                .expectNext(sessaoVotacao(votos, resultado, resumo))
                .verifyComplete();
    }

}