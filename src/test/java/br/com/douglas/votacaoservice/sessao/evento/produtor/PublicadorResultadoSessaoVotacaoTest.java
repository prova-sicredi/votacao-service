package br.com.douglas.votacaoservice.sessao.evento.produtor;

import br.com.douglas.votacaoservice.app.config.PropriedadesProdutorKafka;
import br.com.douglas.votacaoservice.sessao.evento.model.ResultadoSessaoVotacao;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.SettableListenableFuture;

import static br.com.douglas.votacaoservice.fixture.Fixture.TOPICO_RESULTADO_VOTACAO;
import static br.com.douglas.votacaoservice.fixture.Fixture.UUID_SESSAO;
import static br.com.douglas.votacaoservice.fixture.ResultadoSessaoVotacaoFixture.resultadoSessaoVotacao;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PublicadorResultadoSessaoVotacaoTest {

    @Mock
    private PropriedadesProdutorKafka propriedadesProdutorKafka;

    @Mock
    private KafkaTemplate<String, ResultadoSessaoVotacao> kafkaTemplate;

    @InjectMocks
    private PublicadorResultadoSessaoVotacao publicadorResultadoSessaoVotacao;

    @DisplayName("Dado um resultado votacao, deve chamar o kafka template com os parametros corretos")
    @Test
    void deveChamarOKafkaTemplate() {
        doReturn(TOPICO_RESULTADO_VOTACAO).when(propriedadesProdutorKafka).getTopicoResultadoVotacao();
        doReturn(new SettableListenableFuture<SendResult<String, ResultadoSessaoVotacao>>())
                .when(kafkaTemplate).send(TOPICO_RESULTADO_VOTACAO, UUID_SESSAO, resultadoSessaoVotacao());
        publicadorResultadoSessaoVotacao.publicar(resultadoSessaoVotacao());
        verify(kafkaTemplate, times(1)).send(TOPICO_RESULTADO_VOTACAO, UUID_SESSAO, resultadoSessaoVotacao());
    }
}