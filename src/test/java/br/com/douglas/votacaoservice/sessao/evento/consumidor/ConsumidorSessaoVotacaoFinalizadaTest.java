package br.com.douglas.votacaoservice.sessao.evento.consumidor;

import br.com.douglas.votacaoservice.sessao.evento.handler.SessaoVotacaoFinalizadaHandler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

import static br.com.douglas.votacaoservice.fixture.SessaoVotacaoFinalizadaFixture.sessaoVotacaoFinalizada;
import static br.com.douglas.votacaoservice.fixture.SessaoVotacaoFixture.sessaoVotacao;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConsumidorSessaoVotacaoFinalizadaTest {

    @Mock
    private SessaoVotacaoFinalizadaHandler sessaoVotacaoFinalizadaHandler;

    @InjectMocks
    private ConsumidorSessaoVotacaoFinalizada consumidorSessaoVotacaoFinalizada;

    @DisplayName("Dado uma SessaoVotacaoFinalizada, deve chamar o handler com a sessao finalizada")
    @Test
    void deveChamarOHandler() {
        doReturn(Mono.just(sessaoVotacao()))
                .when(sessaoVotacaoFinalizadaHandler).finalizarSessao(sessaoVotacaoFinalizada());
        consumidorSessaoVotacaoFinalizada.listen(sessaoVotacaoFinalizada());
        verify(sessaoVotacaoFinalizadaHandler, times(1)).finalizarSessao(sessaoVotacaoFinalizada());
    }

}