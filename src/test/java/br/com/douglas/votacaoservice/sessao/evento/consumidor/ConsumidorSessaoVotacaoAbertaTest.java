package br.com.douglas.votacaoservice.sessao.evento.consumidor;

import br.com.douglas.votacaoservice.sessao.evento.handler.SessaoVotacaoAbertaHandler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

import static br.com.douglas.votacaoservice.fixture.SessaoVotacaoAbertaFixture.sessaoVotacaoAberta;
import static br.com.douglas.votacaoservice.fixture.SessaoVotacaoFixture.sessaoVotacao;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConsumidorSessaoVotacaoAbertaTest {

    @Mock
    private SessaoVotacaoAbertaHandler sessaoVotacaoAbertaHandler;

    @InjectMocks
    private ConsumidorSessaoVotacaoAberta consumidorSessaoVotacaoAberta;

    @DisplayName("Dado uma SessaoVotacaoAberta, deve chamar o handler passando a sessao de votacao")
    @Test
    void deveChamarOHandler() {
        doReturn(Mono.just(sessaoVotacao())).when(sessaoVotacaoAbertaHandler).abrirSessaoVotacao(sessaoVotacaoAberta());
        consumidorSessaoVotacaoAberta.listen(sessaoVotacaoAberta());
        verify(sessaoVotacaoAbertaHandler, times(1)).abrirSessaoVotacao(sessaoVotacaoAberta());
    }

}