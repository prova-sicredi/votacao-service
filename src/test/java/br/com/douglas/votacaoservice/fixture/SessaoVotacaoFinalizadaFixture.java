package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoFinalizada;

import static br.com.douglas.votacaoservice.fixture.Fixture.UUID_PAUTA;

public class SessaoVotacaoFinalizadaFixture {

    public static SessaoVotacaoFinalizada sessaoVotacaoFinalizada() {
        return SessaoVotacaoFinalizada.builder()
                .uuidPauta(UUID_PAUTA)
                .build();
    }
}
