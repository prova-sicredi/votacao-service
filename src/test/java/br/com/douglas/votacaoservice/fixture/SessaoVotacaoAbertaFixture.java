package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoAberta;

import static br.com.douglas.votacaoservice.fixture.Fixture.*;

public class SessaoVotacaoAbertaFixture {

    public static SessaoVotacaoAberta sessaoVotacaoAberta() {
        return SessaoVotacaoAberta.builder()
                .uuidPauta(UUID_PAUTA)
                .horarioFechamento(_11_11_2021_22_23_00)
                .build();
    }
}
