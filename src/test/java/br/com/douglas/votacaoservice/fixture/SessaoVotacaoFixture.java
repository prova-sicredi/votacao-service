package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.sessao.model.SessaoVotacao;
import br.com.douglas.votacaoservice.votacao.enumeration.OpcaoVoto;

import java.util.Map;

import static br.com.douglas.votacaoservice.fixture.Fixture.*;

public class SessaoVotacaoFixture {

    public static SessaoVotacao sessaoVotacao() {
        return SessaoVotacao.builder()
                .uuidPauta(UUID_PAUTA)
                .uuidSessao(UUID_SESSAO)
                .horarioAbertura(_11_11_2021_22_15_00)
                .horarioFechamento(_11_11_2021_22_18_00)
                .resultado(RESULTADO_VOTACAO)
                .resumoVotacao(RESUMO_VOTACAO)
                .votos(VOTOS)
                .build();
    }

    public static SessaoVotacao sessaoVotacao(Map<String, OpcaoVoto> votos) {
        return SessaoVotacao.builder()
                .uuidPauta(UUID_PAUTA)
                .uuidSessao(UUID_SESSAO)
                .horarioAbertura(_11_11_2021_22_15_00)
                .horarioFechamento(_11_11_2021_22_18_00)
                .votos(votos)
                .build();
    }

    public static SessaoVotacao sessaoVotacao(Map<String, OpcaoVoto> votos, String resultado, String resumo) {
        return SessaoVotacao.builder()
                .uuidPauta(UUID_PAUTA)
                .uuidSessao(UUID_SESSAO)
                .horarioAbertura(_11_11_2021_22_15_00)
                .horarioFechamento(_11_11_2021_22_18_00)
                .resultado(resultado)
                .resumoVotacao(resumo)
                .votos(votos)
                .build();
    }
}
