package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.app.exception.model.GenericException;
import br.com.douglas.votacaoservice.app.exception.model.ResponseError;
import org.springframework.http.HttpStatus;

public class GenericExceptionFixture {

    public static GenericException genericException(HttpStatus status, String mensagem, String mensagemUsuario) {
        return new GenericException(ResponseError.builder()
                        .status(status)
                        .mensagemUsuario(mensagemUsuario)
                        .mensagem(mensagem)
                        .data(Fixture._11_11_2021_22_15_00)
                        .build());
    }
}
