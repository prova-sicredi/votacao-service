package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.sessao.repository.entity.SessaoVotacaoEntity;

import static br.com.douglas.votacaoservice.fixture.Fixture.*;

public class SessaoVotacaoEntityFixture {

    public static SessaoVotacaoEntity sessaoVotacaoEntity() {
        return SessaoVotacaoEntity.builder()
                .uuidPauta(UUID_PAUTA)
                .uuidSessao(UUID_SESSAO)
                .horarioAbertura(_11_11_2021_22_15_00)
                .horarioFechamento(_11_11_2021_22_18_00)
                .resultado(RESULTADO_VOTACAO)
                .resumoVotacao(RESUMO_VOTACAO)
                .votos(VOTOS)
                .build();
    }
}
