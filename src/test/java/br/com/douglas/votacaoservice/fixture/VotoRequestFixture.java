package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.votacao.model.request.VotoRequest;

import static br.com.douglas.votacaoservice.fixture.Fixture.*;

public class VotoRequestFixture {

    public static VotoRequest votoRequest() {
        return VotoRequest.builder()
                .cpf(CPF)
                .opcaoVoto(OPCAO_VOTO)
                .pauta(UUID_PAUTA)
                .build();
    }
}
