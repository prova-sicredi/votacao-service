package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.usuario.enumeration.SituacaoUsuario;
import br.com.douglas.votacaoservice.usuario.model.Usuario;

public class UsuarioFixture {

    public static Usuario usuario(SituacaoUsuario situacaoUsuario) {
        return Usuario.builder()
                .status(situacaoUsuario)
                .build();
    }
}
