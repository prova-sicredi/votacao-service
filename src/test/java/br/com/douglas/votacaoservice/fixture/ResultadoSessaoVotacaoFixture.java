package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.sessao.evento.model.ResultadoSessaoVotacao;

import static br.com.douglas.votacaoservice.fixture.Fixture.*;

public class ResultadoSessaoVotacaoFixture {

    public static ResultadoSessaoVotacao resultadoSessaoVotacao() {
        return ResultadoSessaoVotacao.builder()
                .uuidSessao(UUID_SESSAO)
                .uuidPauta(UUID_PAUTA)
                .resultado(RESULTADO_VOTACAO)
                .resumoVotacao(RESUMO_VOTACAO)
                .build();
    }
}
