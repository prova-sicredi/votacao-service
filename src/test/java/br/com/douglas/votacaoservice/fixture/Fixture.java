package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.votacao.enumeration.OpcaoVoto;
import org.assertj.core.util.Maps;

import java.time.LocalDateTime;
import java.util.Map;

import static java.time.Month.NOVEMBER;

public interface Fixture {

    String UUID_SESSAO = "618dd2efb11ebb248bd97eae";
    String UUID_VOTO = "618c7c2c57a3970db3edefa8";
    String UUID_VOTO_2 = "618c800c57a3970db3edefaa";
    String UUID_PAUTA = "618c7e3457a3970db3edefa9";
    String CPF = "02484231008";
    OpcaoVoto OPCAO_VOTO = OpcaoVoto.SIM;
    String RESULTADO_VOTACAO = "SIM";
    String RESUMO_VOTACAO = "SIM: 1 | NAO: 0";
    String TOPICO_RESULTADO_VOTACAO = "topicoResultadoVotacao";
    Map<String, OpcaoVoto> VOTOS = Maps.newHashMap(CPF, OPCAO_VOTO);

    LocalDateTime _11_11_2021_22_18_00 = LocalDateTime.of(2021, NOVEMBER, 11, 22,18);
    LocalDateTime _11_11_2021_22_23_00 = LocalDateTime.of(2021, NOVEMBER, 11, 22,23);
    LocalDateTime _11_11_2021_22_15_00 = LocalDateTime.of(2021, NOVEMBER, 11, 22,15);
}
