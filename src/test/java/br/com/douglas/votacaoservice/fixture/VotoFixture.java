package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.votacao.model.Voto;

import static br.com.douglas.votacaoservice.fixture.Fixture.*;

public class VotoFixture {

    public static Voto voto() {
        return Voto.builder()
                .uuidVoto(UUID_VOTO)
                .pauta(UUID_PAUTA)
                .opcaoVoto(OPCAO_VOTO)
                .cpf(CPF)
                .horarioInclusao(_11_11_2021_22_15_00)
                .build();
    }

    public static Voto votoNaoSalvo() {
        return Voto.builder()
                .pauta(UUID_PAUTA)
                .opcaoVoto(OPCAO_VOTO)
                .cpf(CPF)
                .horarioInclusao(_11_11_2021_22_15_00)
                .build();
    }
}
