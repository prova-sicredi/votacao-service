package br.com.douglas.votacaoservice.fixture;

import br.com.douglas.votacaoservice.votacao.repository.entity.VotoEntity;

import static br.com.douglas.votacaoservice.fixture.Fixture.*;

public class VotoEntityFixture {

    public static VotoEntity votoEntity() {
        return VotoEntity.builder()
                .uuidVoto(UUID_VOTO)
                .pauta(UUID_PAUTA)
                .opcaoVoto(OPCAO_VOTO)
                .cpf(CPF)
                .horarioInclusao(_11_11_2021_22_15_00)
                .build();
    }
}
