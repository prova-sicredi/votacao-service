package br.com.douglas.votacaoservice.usuario.enumeration;

public enum SituacaoUsuario {
    ABLE_TO_VOTE,
    UNABLE_TO_VOTE
}
