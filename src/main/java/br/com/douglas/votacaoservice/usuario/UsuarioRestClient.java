package br.com.douglas.votacaoservice.usuario;

import br.com.douglas.votacaoservice.usuario.model.Usuario;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface UsuarioRestClient {
    Mono<Usuario> consultarSituacaoUsuario(String cpf);
}
