package br.com.douglas.votacaoservice.usuario.model;

import br.com.douglas.votacaoservice.usuario.enumeration.SituacaoUsuario;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Usuario {
    private SituacaoUsuario status;
}
