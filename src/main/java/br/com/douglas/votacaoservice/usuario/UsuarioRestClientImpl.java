package br.com.douglas.votacaoservice.usuario;

import br.com.douglas.votacaoservice.app.exception.GenericExceptionBuilder;
import br.com.douglas.votacaoservice.app.exception.model.GenericException;
import br.com.douglas.votacaoservice.usuario.model.Usuario;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class UsuarioRestClientImpl implements UsuarioRestClient {

    private static final String MENSAGEM_ERRO = "Ocorreu erro ao tentar validar a situação do usuário";
    private static final String MENSAGEM_ERRO_USUARIO = "Tente novamente dentro de alguns minutos.";

    private final WebClient webClientUsuario;
    private final GenericExceptionBuilder genericExceptionBuilder;

    @Override
    public Mono<Usuario> consultarSituacaoUsuario(String cpf) {
        return webClientUsuario.get()
                .uri(uriBuilder -> uriBuilder.path("/users/{cpf}")
                        .build(cpf))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::buildClientError)
                .onStatus(HttpStatus::is5xxServerError, this::buildClientError)
                .bodyToMono(Usuario.class);
    }

    private Mono<GenericException> buildClientError(ClientResponse exception) {
        return Mono.error(genericExceptionBuilder.buildGenericException(MENSAGEM_ERRO,exception.statusCode(),
                MENSAGEM_ERRO_USUARIO));
    }
}
