package br.com.douglas.votacaoservice.votacao.repository.mapper;

import br.com.douglas.votacaoservice.votacao.model.Voto;
import br.com.douglas.votacaoservice.votacao.repository.entity.VotoEntity;
import reactor.core.publisher.Mono;

public class VotoEntityMapper {

    public static Mono<VotoEntity> toVotoEntity(Voto voto) {
        return Mono.just(VotoEntity.builder()
                .uuidVoto(voto.getUuidVoto())
                .horarioInclusao(voto.getHorarioInclusao())
                .pauta(voto.getPauta())
                .cpf(voto.getCpf())
                .opcaoVoto(voto.getOpcaoVoto())
                .build());
    }

    public static Mono<Voto> toVoto(VotoEntity votoEntity) {
        return Mono.just(Voto.builder()
                .uuidVoto(votoEntity.getUuidVoto())
                .horarioInclusao(votoEntity.getHorarioInclusao())
                .cpf(votoEntity.getCpf())
                .pauta(votoEntity.getPauta())
                .opcaoVoto(votoEntity.getOpcaoVoto())
                .build());
    }
}
