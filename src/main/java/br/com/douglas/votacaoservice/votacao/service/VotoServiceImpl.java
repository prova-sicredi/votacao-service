package br.com.douglas.votacaoservice.votacao.service;

import br.com.douglas.votacaoservice.app.exception.GenericExceptionBuilder;
import br.com.douglas.votacaoservice.sessao.service.SessaoVotacaoService;
import br.com.douglas.votacaoservice.usuario.UsuarioRestClient;
import br.com.douglas.votacaoservice.usuario.enumeration.SituacaoUsuario;
import br.com.douglas.votacaoservice.usuario.model.Usuario;
import br.com.douglas.votacaoservice.votacao.mapper.VotoMapper;
import br.com.douglas.votacaoservice.votacao.model.Voto;
import br.com.douglas.votacaoservice.votacao.model.request.VotoRequest;
import br.com.douglas.votacaoservice.votacao.repository.VotoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class VotoServiceImpl implements VotoService {

    private static final String MENSAGEM_USUARIO_INAPTO = "Usuário %s não está apto para votar";
    private static final String MENSAGEM_TENTE_NOVAMENTE = "Tente com outro usuário";

    private final UsuarioRestClient usuarioRestClient;
    private final GenericExceptionBuilder genericExceptionBuilder;
    private final SessaoVotacaoService sessaoVotacaoService;
    private final VotoMapper votoMapper;
    private final VotoRepository votoRepository;

    @Override
    public Mono<String> registrarVoto(VotoRequest votoRequest) {
        return usuarioRestClient.consultarSituacaoUsuario(votoRequest.getCpf())
                .filter(this::usuarioEstaAptoAVotar)
                .switchIfEmpty(gerarErroUsuarioInapto(votoRequest))
                .then(votoMapper.toVoto(votoRequest))
                .flatMap(sessaoVotacaoService::adicionarVotoASessao)
                .flatMap(votoRepository::salvar)
                .map(Voto::getUuidVoto);
    }

    private boolean usuarioEstaAptoAVotar(Usuario usuario) {
        return SituacaoUsuario.ABLE_TO_VOTE.equals(usuario.getStatus());
    }

    private Mono<Usuario> gerarErroUsuarioInapto(VotoRequest votoRequest) {
        return Mono.error(genericExceptionBuilder.buildGenericException(format(MENSAGEM_USUARIO_INAPTO,
                votoRequest.getCpf()), HttpStatus.UNPROCESSABLE_ENTITY, MENSAGEM_TENTE_NOVAMENTE));
    }


}
