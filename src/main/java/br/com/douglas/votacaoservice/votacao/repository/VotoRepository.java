package br.com.douglas.votacaoservice.votacao.repository;

import br.com.douglas.votacaoservice.votacao.model.Voto;
import reactor.core.publisher.Mono;

public interface VotoRepository {
    Mono<Voto> salvar(Voto voto);
}
