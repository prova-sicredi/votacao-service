package br.com.douglas.votacaoservice.votacao.repository;

import br.com.douglas.votacaoservice.votacao.repository.entity.VotoEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotoMongoRepository extends ReactiveMongoRepository<VotoEntity, String> {
}
