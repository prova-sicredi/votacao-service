package br.com.douglas.votacaoservice.votacao.enumeration;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum OpcaoVoto {
    SIM,
    NAO;

    public static Map<OpcaoVoto, Integer> gerarContador(){
        return Stream.of(OpcaoVoto.values())
                .collect(Collectors.toMap(opcaoVoto -> opcaoVoto, operacaoVoto -> 0));
    }
}
