package br.com.douglas.votacaoservice.votacao.mapper;

import br.com.douglas.votacaoservice.app.provider.DateTimeProvider;
import br.com.douglas.votacaoservice.votacao.model.Voto;
import br.com.douglas.votacaoservice.votacao.model.request.VotoRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class VotoMapper {

    private final DateTimeProvider dateTimeProvider;

    public Mono<Voto> toVoto(VotoRequest votoRequest) {
        return Mono.just(Voto.builder()
                .cpf(votoRequest.getCpf())
                .opcaoVoto(votoRequest.getOpcaoVoto())
                .horarioInclusao(dateTimeProvider.now())
                .pauta(votoRequest.getPauta())
                .build());
    }
}
