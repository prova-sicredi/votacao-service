package br.com.douglas.votacaoservice.votacao.repository.entity;

import br.com.douglas.votacaoservice.votacao.enumeration.OpcaoVoto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "voto")
public class VotoEntity {
    @Id
    private String uuidVoto;
    private OpcaoVoto opcaoVoto;
    private String cpf;
    private LocalDateTime horarioInclusao;
    private String pauta;
}
