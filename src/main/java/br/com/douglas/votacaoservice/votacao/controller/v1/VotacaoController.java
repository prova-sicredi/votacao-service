package br.com.douglas.votacaoservice.votacao.controller.v1;

import br.com.douglas.votacaoservice.app.exception.model.ResponseError;
import br.com.douglas.votacaoservice.votacao.model.request.VotoRequest;
import br.com.douglas.votacaoservice.votacao.service.VotoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

import static br.com.douglas.votacaoservice.app.constants.SwaggerTags.VOTOS_V1;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/votos")
@Api(tags = {VOTOS_V1}, produces = MediaType.APPLICATION_JSON_VALUE)
public class VotacaoController {

    private final VotoService votoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Cadastrar um voto")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Voto cadastrado", response = String.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro interno do servidor", response = ResponseError.class)
    })
    public Mono<String> cadastrarVoto(@Valid @RequestBody  Mono<VotoRequest> votoRequest) {
        return votoRequest.flatMap(votoService::registrarVoto);
    }

}
