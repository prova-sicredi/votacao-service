package br.com.douglas.votacaoservice.votacao.service;

import br.com.douglas.votacaoservice.votacao.model.request.VotoRequest;
import reactor.core.publisher.Mono;

public interface VotoService {
    Mono<String> registrarVoto(VotoRequest votoRequest);
}
