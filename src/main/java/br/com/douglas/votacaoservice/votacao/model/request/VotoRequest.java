package br.com.douglas.votacaoservice.votacao.model.request;

import br.com.caelum.stella.bean.validation.CPF;
import br.com.douglas.votacaoservice.votacao.enumeration.OpcaoVoto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Data
@Builder
@NoArgsConstructor
@ApiModel
public class VotoRequest {

    @NotBlank(message = "CPF não deve estar vazio.")
    @CPF(message = "CPF deve conter um CPF válido.")
    @ApiParam(required = true, example = "02868536042")
    private String cpf;

    @NotNull(message = "A opção de voto é obrigatória.")
    @ApiParam(required = true, example = "SIM", allowableValues = "SIM, NAO")
    private OpcaoVoto opcaoVoto;

    @NotBlank(message = "A pauta não deve estar vazia.")
    @ApiParam(required = true, example = "edc04c392f0b4dc58a54b012136ee9c9")
    private String pauta;

}
