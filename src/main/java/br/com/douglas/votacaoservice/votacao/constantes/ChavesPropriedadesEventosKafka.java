package br.com.douglas.votacaoservice.votacao.constantes;

public interface ChavesPropriedadesEventosKafka {

    String EVENTO_SESSAO_VOTACAO_ABERTA_CONSUMER_FACTORY = "eventoSessaoVotacaoAbertaConsumerFactory";
    String EVENTO_SESSAO_VOTACAO_ABERTA_KAFKA_LISTENER_CONTAINER_FACTORY = "eventoSessaoVotacaoAbertaKafkaListenerContainerFactory";
    String EVENTO_SESSAO_VOTACAO_FINALIZADA_CONSUMER_FACTORY = "eventoSessaoVotacaoFinalizadaConsumerFactory";
    String EVENTO_SESSAO_VOTACAO_FINALIZADA_KAFKA_LISTENER_CONTAINER_FACTORY = "eventoSessaoVotacaoFinalizadaKafkaListenerContainerFactory";

}
