package br.com.douglas.votacaoservice.votacao.model;

import br.com.douglas.votacaoservice.votacao.enumeration.OpcaoVoto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Builder
@Data
@AllArgsConstructor
public class Voto {
    private String uuidVoto;
    private String cpf;
    private OpcaoVoto opcaoVoto;
    private LocalDateTime horarioInclusao;
    private String pauta;
}
