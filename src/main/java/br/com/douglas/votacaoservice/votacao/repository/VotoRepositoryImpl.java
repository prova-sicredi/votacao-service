package br.com.douglas.votacaoservice.votacao.repository;

import br.com.douglas.votacaoservice.app.exception.GenericExceptionBuilder;
import br.com.douglas.votacaoservice.votacao.model.Voto;
import br.com.douglas.votacaoservice.votacao.repository.mapper.VotoEntityMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class VotoRepositoryImpl implements VotoRepository {

    private static final String MENSAGEM_ERRO_BANCO = "Ocorreu um erro ao salvar o voto no banco";
    private static final String MENSAGEM_USUARIO_ERRO_BANCO = "Tente novamente dentro de alguns minutos.";

    private final VotoMongoRepository votoMongoRepository;
    private final GenericExceptionBuilder genericExceptionBuilder;

    @Override
    public Mono<Voto> salvar(Voto voto) {
        return VotoEntityMapper.toVotoEntity(voto)
                .flatMap(votoMongoRepository::save)
                .switchIfEmpty(Mono.error(genericExceptionBuilder
                        .buildGenericException(MENSAGEM_ERRO_BANCO, HttpStatus.BAD_GATEWAY,
                                MENSAGEM_USUARIO_ERRO_BANCO)))
                .flatMap(VotoEntityMapper::toVoto);
    }
}
