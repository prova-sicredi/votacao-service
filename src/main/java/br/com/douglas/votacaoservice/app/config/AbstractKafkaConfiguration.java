package br.com.douglas.votacaoservice.app.config;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.apache.kafka.clients.producer.ProducerConfig.*;
import static org.springframework.kafka.support.serializer.JsonSerializer.ADD_TYPE_INFO_HEADERS;

@RequiredArgsConstructor
public abstract class AbstractKafkaConfiguration {

    protected final PropriedadesKafka propriedadesKafka;

    protected Map<String, Object> configuracoesProdutor() {
        PropriedadesProdutorKafka propriedades = propriedadesKafka.getPropriedadesProdutorKafka();
        Map<String, Object> propriedadesProdutor = new HashMap<>();
        propriedadesProdutor.put(ACKS_CONFIG, propriedades.getAcks());
        propriedadesProdutor.put(ADD_TYPE_INFO_HEADERS, false);
        propriedadesProdutor.put(BATCH_SIZE_CONFIG, 16_384 * 4);
        propriedadesProdutor.put(BOOTSTRAP_SERVERS_CONFIG, propriedadesKafka.getBootstrapServers());
        propriedadesProdutor.put(COMPRESSION_TYPE_CONFIG, propriedades.getCompressionType());
        propriedadesProdutor.put(KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        propriedadesProdutor.put(LINGER_MS_CONFIG, propriedades.getLingerMs());
        propriedadesProdutor.put(REQUEST_TIMEOUT_MS_CONFIG, propriedades.getRequestTimeoutMs());
        propriedadesProdutor.put(RETRIES_CONFIG, propriedades.getRetries());
        propriedadesProdutor.put(RETRY_BACKOFF_MS_CONFIG, propriedades.getRetryBackOffMs());
        propriedadesProdutor.put(VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        if (propriedades.isEnableIdempotence()) {
            propriedadesProdutor.put(ENABLE_IDEMPOTENCE_CONFIG, true);
            propriedadesProdutor.put(TRANSACTIONAL_ID_CONFIG, propriedades.getTransactionalIdSufix() + UUID.randomUUID().toString());
        }
        return propriedadesProdutor;
    }

    protected Map<String, Object> configuracoesConsumidor() {
        Map<String, Object> propriedadesConsumidor = new HashMap<>();
        propriedadesConsumidor.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, propriedadesKafka.getBootstrapServers());
        propriedadesConsumidor.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        propriedadesConsumidor.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        return propriedadesConsumidor;
    }
}
