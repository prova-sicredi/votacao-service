package br.com.douglas.votacaoservice.app.constants;

public interface SwaggerTags {
    String VOTOS_V1 = "votosV1";
    String DESCRICAO_VOTOS_V1 = "V1 da API de votos";
}
