package br.com.douglas.votacaoservice.app.exception.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class GenericException extends RuntimeException {
    private ResponseError responseError;

    public GenericException(ResponseError responseError) {
        super(responseError.getMensagem());
        this.responseError = responseError;
    }
}
