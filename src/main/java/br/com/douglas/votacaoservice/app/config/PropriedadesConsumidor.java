package br.com.douglas.votacaoservice.app.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static br.com.douglas.votacaoservice.app.constants.ChavesConfiguracoes.*;

@Getter
@Component
public class PropriedadesConsumidor {

    @Value(RETRIES)
    private Integer numeroTentativas;

    @Value(REQUEST_TIMEOUT_MS)
    private Integer intervaloRetentativasMs;

    @Value(TOPICO_SESSAO_ABERTA)
    private String topicoSessaoAberta;

    @Value(TOPICO_SESSAO_ABERTA_ERROR)
    private String topicoSessaoAbertaError;

    @Value(TOPICO_SESSAO_FINALIZADA)
    private String topicoSessaoFinalizada;

    @Value(TOPICO_SESSAO_FINALIZADA_ERROR)
    private String topicoSessaoFinalizadaError;
}
