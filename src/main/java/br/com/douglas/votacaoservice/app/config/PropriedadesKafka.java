package br.com.douglas.votacaoservice.app.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static br.com.douglas.votacaoservice.app.constants.ChavesConfiguracoes.*;

@Getter
@Component
@RequiredArgsConstructor
public class PropriedadesKafka {

    @Value(BROKERS)
    private String bootstrapServers;
    @Value(AUTO_COMMIT)
    private boolean autoCommit;
    @Value(ISOLATION_LEVEL)
    private String isolationLevel;
    @Value(FETCH_MAX_WAIT_MS_CONFIG)
    private String fetchMaxWaitMs;
    @Value(AUTO_OFFSET_RESET_CONFIG)
    private String autoOffsetReset;
    @Value(APPLICATION_ID_CONFIG)
    private String applicationIdConfig;
    private final PropriedadesProdutorKafka propriedadesProdutorKafka;
    private final PropriedadesConsumidor proriedadesConsumidor;

}
