package br.com.douglas.votacaoservice.app.exception;

import br.com.douglas.votacaoservice.app.exception.model.GenericException;
import br.com.douglas.votacaoservice.app.exception.model.ResponseError;
import br.com.douglas.votacaoservice.app.provider.DateTimeProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@RequiredArgsConstructor
public class GenericExceptionBuilder {

    private final DateTimeProvider dateTimeProvider;

    public GenericException buildGenericException(String message, HttpStatus status, String... mensagensUsuario) {
        return new GenericException(ResponseError.builder()
                .mensagensUsuario(Arrays.asList(mensagensUsuario))
                .mensagem(message)
                .status(status)
                .data(dateTimeProvider.now())
                .build());
    }
}
