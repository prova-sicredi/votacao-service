package br.com.douglas.votacaoservice.app.exception;

import br.com.douglas.votacaoservice.app.exception.model.GenericException;
import br.com.douglas.votacaoservice.app.exception.model.ResponseError;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebInputException;

import java.time.LocalDateTime;

@ControllerAdvice
@Configuration
public class GenericExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseError> handleException(Exception exception) {
        ResponseError responseError = ResponseError.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .mensagem(exception.getMessage())
                .mensagemUsuario("Ocorreu um erro inesperado")
                .data(LocalDateTime.now())
                .build();
        return new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity<ResponseError> handleWebExchangeBindException(WebExchangeBindException exception) {
        ResponseError.ResponseErrorBuilder responseErrorBuilder = ResponseError.builder()
                .status(exception.getStatus())
                .mensagem("Requisição mal formatada.")
                .data(LocalDateTime.now());
        exception.getBindingResult()
                .getAllErrors()
                .forEach(erro -> responseErrorBuilder.mensagemUsuario(erro.getDefaultMessage()));
        return new ResponseEntity<>(responseErrorBuilder.build(), exception.getStatus());
    }

    @ExceptionHandler(ServerWebInputException.class)
    public ResponseEntity<ResponseError> handleWebExchangeBindException(ServerWebInputException exception) {
        Throwable cause = exception.getCause();
        ResponseError.ResponseErrorBuilder responseErrorBuilder = ResponseError.builder()
                .status(exception.getStatus())
                .mensagemUsuario(cause.getMessage())
                .mensagem("Erro na formatação da requisição.")
                .data(LocalDateTime.now());
        return new ResponseEntity<>(responseErrorBuilder.build(), exception.getStatus());
    }

    @ExceptionHandler(GenericException.class)
    public ResponseEntity<ResponseError> handleGenericException(GenericException exception) {
        ResponseError responseError = exception.getResponseError();
        if (ObjectUtils.isEmpty(responseError.getData())) {
            responseError.setData(LocalDateTime.now());
        }
        return new ResponseEntity<>(responseError, responseError.getStatus());
    }
}
