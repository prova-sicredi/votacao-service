package br.com.douglas.votacaoservice.app.exception.model;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseError {
    private String mensagem;
    private HttpStatus status;
    @Singular(value = "mensagemUsuario")
    private List<String> mensagensUsuario;
    private LocalDateTime data;
}
