package br.com.douglas.votacaoservice.app.constants;

public interface ChavesConfiguracoes {
    String BROKERS = "${votacao.cloud.kafka.binder.brokers}";
    String ISOLATION_LEVEL = "${votacao.cloud.kafka.isolation-level:read_committed}";
    String AUTO_COMMIT = "${votacao.cloud.kafka.auto-commit:false}";
    String APPLICATION_ID_CONFIG = "${votacao.cloud.kafka.application-id:votacao-service}";
    String FETCH_MAX_WAIT_MS_CONFIG = "${votacao.cloud.kafka.fetch.max.wait.ms:}";
    String AUTO_OFFSET_RESET_CONFIG = "${votacao.cloud.kafka.auto.offset.reset:}";
    String ACKS = "${votacao.cloud.kafka.acks}";
    String COMPRESSION_TYPE = "${votacao.cloud.kafka.compression-type}";
    String ENABLE_IDEMPOTENCE = "${votacao.cloud.kafka.enable-idempotence}";
    String LINGER_MS = "${votacao.cloud.kafka.linger-ms}";
    String REQUEST_TIMEOUT_MS = "${votacao.cloud.kafka.request-timeout-ms}";
    String RETRIES = "${votacao.cloud.kafka.retries}";
    String RETRY_BACKOFF_MS = "${votacao.cloud.kafka.retry-backoff-ms}";
    String TRANSACTION_ID_SUFIX = "${votacao.cloud.kafka.transaction-id-sufix}";
    String TOPICO_SESSAO_ABERTA = "${votacao.cloud.kafka.topics.sessao-aberta.topico}";
    String TOPICO_SESSAO_ABERTA_ERROR = "${votacao.cloud.kafka.topics.sessao-aberta.topico-erro}";
    String TOPICO_SESSAO_FINALIZADA = "${votacao.cloud.kafka.topics.sessao-finalizada.topico}";
    String TOPICO_SESSAO_FINALIZADA_ERROR = "${votacao.cloud.kafka.topics.sessao-finalizada.topico-erro}";
    String TOPICO_RESULTADO_VOTACAO = "${votacao.cloud.kafka.topics.resultado-votacao.topico}";

}
