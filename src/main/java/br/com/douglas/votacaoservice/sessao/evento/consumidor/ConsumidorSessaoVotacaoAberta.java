package br.com.douglas.votacaoservice.sessao.evento.consumidor;

import br.com.douglas.votacaoservice.sessao.evento.handler.SessaoVotacaoAbertaHandler;
import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoAberta;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import static br.com.douglas.votacaoservice.app.constants.ChavesConfiguracoes.APPLICATION_ID_CONFIG;
import static br.com.douglas.votacaoservice.app.constants.ChavesConfiguracoes.TOPICO_SESSAO_ABERTA;
import static br.com.douglas.votacaoservice.votacao.constantes.ChavesPropriedadesEventosKafka.EVENTO_SESSAO_VOTACAO_ABERTA_KAFKA_LISTENER_CONTAINER_FACTORY;

@Component
@RequiredArgsConstructor
@Slf4j
public class ConsumidorSessaoVotacaoAberta {

    private final SessaoVotacaoAbertaHandler sessaoVotacaoAbertaHandler;

    @KafkaListener(
            topics = TOPICO_SESSAO_ABERTA,
            groupId = APPLICATION_ID_CONFIG,
            containerFactory = EVENTO_SESSAO_VOTACAO_ABERTA_KAFKA_LISTENER_CONTAINER_FACTORY
    )
    public void listen(@Payload SessaoVotacaoAberta sessaoVotacaoAberta) {
        sessaoVotacaoAbertaHandler.abrirSessaoVotacao(sessaoVotacaoAberta)
                .doOnSuccess(sessao -> log.info(String.format("Sessao de votação %s aberta com sucesso", sessao.getUuidSessao())))
                .subscribe();
    }

}
