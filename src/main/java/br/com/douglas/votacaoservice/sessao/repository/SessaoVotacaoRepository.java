package br.com.douglas.votacaoservice.sessao.repository;

import br.com.douglas.votacaoservice.sessao.model.SessaoVotacao;
import reactor.core.publisher.Mono;

public interface SessaoVotacaoRepository {
    Mono<SessaoVotacao> salvar(SessaoVotacao sessaoVotacao);
    Mono<SessaoVotacao> buscarPorUuidPauta(String uuidPauta);
}
