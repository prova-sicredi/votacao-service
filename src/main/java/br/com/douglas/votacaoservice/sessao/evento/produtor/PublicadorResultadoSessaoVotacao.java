package br.com.douglas.votacaoservice.sessao.evento.produtor;

import br.com.douglas.votacaoservice.app.config.PropriedadesProdutorKafka;
import br.com.douglas.votacaoservice.sessao.evento.model.ResultadoSessaoVotacao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.SuccessCallback;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
@Slf4j
public class PublicadorResultadoSessaoVotacao {

    private static final String LOG_PUBLICACAO = "Enviado Evento de resultado da votação com sucesso topico={}, offset={}, partition={}}: {}.";
    private static final String LOG_ERRO_PUBLICACAO = "Erro ao enviar Evento de resultado da votação para tópico=%s: %s.";

    private final PropriedadesProdutorKafka propriedadesProdutorKafka;
    private final KafkaTemplate<String, ResultadoSessaoVotacao> kafkaTemplateResultadoVotacao;


    public void publicar(ResultadoSessaoVotacao resultadoSessaoVotacao) {
        kafkaTemplateResultadoVotacao.send(propriedadesProdutorKafka.getTopicoResultadoVotacao(),
                resultadoSessaoVotacao.getUuidSessao(),
                resultadoSessaoVotacao).addCallback(
                callbackEnvioComSucesso(resultadoSessaoVotacao),
                callbackErroEnvio(resultadoSessaoVotacao)
        );
    }

    private SuccessCallback<SendResult<String, ResultadoSessaoVotacao>> callbackEnvioComSucesso(ResultadoSessaoVotacao evento) {
        return result -> log.debug(
                LOG_PUBLICACAO,
                propriedadesProdutorKafka.getTopicoPautaFinalizada(),
                result.getRecordMetadata().offset(),
                evento.getUuidPauta(),
                evento
        );
    }

    private FailureCallback callbackErroEnvio(ResultadoSessaoVotacao evento) {
        return error -> log.error(
                format(
                        LOG_ERRO_PUBLICACAO,
                        propriedadesProdutorKafka.getTopicoPautaFinalizada(),
                        evento
                ),
                error
        );
    }
}
