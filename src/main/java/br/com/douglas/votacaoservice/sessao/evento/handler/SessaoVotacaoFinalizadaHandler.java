package br.com.douglas.votacaoservice.sessao.evento.handler;

import br.com.douglas.votacaoservice.sessao.evento.model.ResultadoSessaoVotacao;
import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoFinalizada;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface SessaoVotacaoFinalizadaHandler {
    Mono<ResultadoSessaoVotacao> finalizarSessao(SessaoVotacaoFinalizada sessaoVotacaoFinalizada);
}
