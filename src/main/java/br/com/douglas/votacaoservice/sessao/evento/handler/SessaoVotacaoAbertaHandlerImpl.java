package br.com.douglas.votacaoservice.sessao.evento.handler;

import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoAberta;
import br.com.douglas.votacaoservice.sessao.mapper.SessaoVotacaoMapper;
import br.com.douglas.votacaoservice.sessao.model.SessaoVotacao;
import br.com.douglas.votacaoservice.sessao.service.SessaoVotacaoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Component
public class SessaoVotacaoAbertaHandlerImpl implements SessaoVotacaoAbertaHandler {

    private final SessaoVotacaoService sessaoVotacaoService;
    private final SessaoVotacaoMapper sessaoVotacaoMapper;

    @Override
    public Mono<SessaoVotacao> abrirSessaoVotacao(SessaoVotacaoAberta sessaoVotacaoAberta) {
        return sessaoVotacaoMapper.toSessaoVotacao(sessaoVotacaoAberta)
                .flatMap(sessaoVotacaoService::abrirSessao);
    }
}
