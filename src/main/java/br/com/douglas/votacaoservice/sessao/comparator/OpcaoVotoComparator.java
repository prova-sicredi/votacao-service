package br.com.douglas.votacaoservice.sessao.comparator;

import br.com.douglas.votacaoservice.votacao.enumeration.OpcaoVoto;

import java.util.Comparator;
import java.util.Map;

public class OpcaoVotoComparator implements Comparator<Map.Entry<OpcaoVoto, Integer>> {

    @Override
    public int compare(Map.Entry<OpcaoVoto, Integer> primeiraOpcao, Map.Entry<OpcaoVoto, Integer> segundaOpcao) {
        return segundaOpcao.getValue().compareTo(primeiraOpcao.getValue());
    }
}
