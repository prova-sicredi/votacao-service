package br.com.douglas.votacaoservice.sessao.evento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SessaoVotacaoAberta {
    private String uuidPauta;
    private LocalDateTime horarioFechamento;
}
