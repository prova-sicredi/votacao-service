package br.com.douglas.votacaoservice.sessao.evento.config;

import br.com.douglas.votacaoservice.app.config.AbstractKafkaConfiguration;
import br.com.douglas.votacaoservice.app.config.PropriedadesKafka;
import br.com.douglas.votacaoservice.sessao.evento.model.ResultadoSessaoVotacao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class ConfiguracaoProdutorResultadoVotacao extends AbstractKafkaConfiguration {

    public ConfiguracaoProdutorResultadoVotacao(PropriedadesKafka propriedadesKafka) {
        super(propriedadesKafka);
    }

    @Bean
    public KafkaTemplate<String, ResultadoSessaoVotacao> kafkaTemplateResultadoVotacao() {
        return new KafkaTemplate<>(resultadoVotacaofkaProducerFactory());
    }

    public ProducerFactory<String, ResultadoSessaoVotacao> resultadoVotacaofkaProducerFactory() {
        return new DefaultKafkaProducerFactory<>(configuracoesProdutor());
    }
}
