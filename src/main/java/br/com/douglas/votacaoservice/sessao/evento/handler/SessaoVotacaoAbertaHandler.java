package br.com.douglas.votacaoservice.sessao.evento.handler;

import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoAberta;
import br.com.douglas.votacaoservice.sessao.model.SessaoVotacao;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface SessaoVotacaoAbertaHandler {
    Mono<SessaoVotacao> abrirSessaoVotacao(SessaoVotacaoAberta sessaoVotacaoAberta);
}
