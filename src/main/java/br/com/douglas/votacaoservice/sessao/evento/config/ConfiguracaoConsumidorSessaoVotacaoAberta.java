package br.com.douglas.votacaoservice.sessao.evento.config;

import br.com.douglas.votacaoservice.app.config.AbstractKafkaConfiguration;
import br.com.douglas.votacaoservice.app.config.PropriedadesConsumidor;
import br.com.douglas.votacaoservice.app.config.PropriedadesKafka;
import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoAberta;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.*;
import org.springframework.kafka.support.SendResult;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.retry.RetryContext;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.SuccessCallback;

import static br.com.douglas.votacaoservice.votacao.constantes.ChavesPropriedadesEventosKafka.EVENTO_SESSAO_VOTACAO_ABERTA_CONSUMER_FACTORY;
import static br.com.douglas.votacaoservice.votacao.constantes.ChavesPropriedadesEventosKafka.EVENTO_SESSAO_VOTACAO_ABERTA_KAFKA_LISTENER_CONTAINER_FACTORY;
import static java.util.Optional.empty;
import static org.springframework.kafka.listener.adapter.RetryingMessageListenerAdapter.CONTEXT_RECORD;

@Slf4j
@Component
public class ConfiguracaoConsumidorSessaoVotacaoAberta extends AbstractKafkaConfiguration {

    private final KafkaTemplate<String, SessaoVotacaoAberta> kafkaTemplate;

    public ConfiguracaoConsumidorSessaoVotacaoAberta(PropriedadesKafka propriedadesKafka,
                                                     KafkaTemplate<String, SessaoVotacaoAberta> kafkaTemplate) {
        super(propriedadesKafka);
        this.kafkaTemplate = kafkaTemplate;
    }

    @Bean(EVENTO_SESSAO_VOTACAO_ABERTA_KAFKA_LISTENER_CONTAINER_FACTORY)
    public ConcurrentKafkaListenerContainerFactory<String, SessaoVotacaoAberta> eventoSessaoVotacaoAbertaListener(
            ConsumerFactory<String, SessaoVotacaoAberta> consumerFactory) {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, SessaoVotacaoAberta>();
        factory.setConsumerFactory(consumerFactory);
        factory.setRecoveryCallback(this::recoveryCallback);
        return factory;
    }

    @Bean(EVENTO_SESSAO_VOTACAO_ABERTA_CONSUMER_FACTORY)
    public ConsumerFactory<String, SessaoVotacaoAberta> eventoSessaoVotacaoAbertaConsumerFactory() {
        return new DefaultKafkaConsumerFactory<>(
                configuracoesConsumidor(),
                new StringDeserializer(),
                new JsonDeserializer<>(SessaoVotacaoAberta.class)
        );
    }

    private Object recoveryCallback(RetryContext context) {
        var record = (ConsumerRecord<String, SessaoVotacaoAberta>) context.getAttribute(CONTEXT_RECORD);
        SessaoVotacaoAberta evento = record.value();
        PropriedadesConsumidor proriedadesConsumidor = propriedadesKafka.getProriedadesConsumidor();
        kafkaTemplate.send(proriedadesConsumidor.getTopicoSessaoAbertaError(), record.key(), evento)
                .addCallback(
                        callbackEnvioComSucesso(evento),
                        callbackErroEnvio(evento)
                );
        return empty();
    }

    private SuccessCallback<SendResult<String, SessaoVotacaoAberta>> callbackEnvioComSucesso(SessaoVotacaoAberta evento) {
        return result -> log.warn("Enviado SessaoVotacaoAberta para DLQ: {}.", evento);
    }

    private FailureCallback callbackErroEnvio(SessaoVotacaoAberta evento) {
        return error -> log.error("Erro ao enviar SessaoVotacaoAberta para DLQ: " + evento, error);
    }
}
