package br.com.douglas.votacaoservice.sessao.evento.config;

import br.com.douglas.votacaoservice.app.config.AbstractKafkaConfiguration;
import br.com.douglas.votacaoservice.app.config.PropriedadesKafka;
import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoAberta;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class ConfiguracaoProdutorSessaoVotacaoAberta extends AbstractKafkaConfiguration {

    public ConfiguracaoProdutorSessaoVotacaoAberta(PropriedadesKafka propriedadesKafka) {
        super(propriedadesKafka);
    }

    @Bean
    public KafkaTemplate<String, SessaoVotacaoAberta> kafkaTemplateSessaoAberta() {
        return new KafkaTemplate<>(sessaoAbertaKafkaProducerFactory());
    }

    public ProducerFactory<String, SessaoVotacaoAberta> sessaoAbertaKafkaProducerFactory() {
        return new DefaultKafkaProducerFactory<>(configuracoesProdutor());
    }
}
