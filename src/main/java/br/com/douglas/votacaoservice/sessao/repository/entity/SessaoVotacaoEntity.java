package br.com.douglas.votacaoservice.sessao.repository.entity;

import br.com.douglas.votacaoservice.votacao.enumeration.OpcaoVoto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "sessao-votacao")
public class SessaoVotacaoEntity {
    @Id
    private String uuidSessao;
    private String uuidPauta;
    private LocalDateTime horarioAbertura;
    private LocalDateTime horarioFechamento;
    private Map<String, OpcaoVoto> votos;
    private String resultado;
    private String resumoVotacao;
}
