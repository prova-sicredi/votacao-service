package br.com.douglas.votacaoservice.sessao.mapper;

import br.com.douglas.votacaoservice.app.provider.DateTimeProvider;
import br.com.douglas.votacaoservice.sessao.evento.model.ResultadoSessaoVotacao;
import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoAberta;
import br.com.douglas.votacaoservice.sessao.model.SessaoVotacao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.HashMap;

@Component
@RequiredArgsConstructor
public class SessaoVotacaoMapper {

    private final DateTimeProvider dateTimeProvider;

    public Mono<SessaoVotacao> toSessaoVotacao(SessaoVotacaoAberta sessaoVotacaoAberta) {
        return Mono.just(SessaoVotacao.builder()
                .uuidPauta(sessaoVotacaoAberta.getUuidPauta())
                .horarioFechamento(sessaoVotacaoAberta.getHorarioFechamento())
                .votos(new HashMap<>())
                .horarioAbertura(dateTimeProvider.now())
                .build());
    }

    public Mono<ResultadoSessaoVotacao> toResultadoSessaoVotacao(SessaoVotacao sessaoVotacao) {
        return Mono.just(ResultadoSessaoVotacao.builder()
                .uuidSessao(sessaoVotacao.getUuidSessao())
                .uuidPauta(sessaoVotacao.getUuidPauta())
                .resultado(sessaoVotacao.getResultado())
                .resumoVotacao(sessaoVotacao.getResumoVotacao())
                .build());
    }

}
