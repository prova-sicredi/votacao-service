package br.com.douglas.votacaoservice.sessao.repository;

import br.com.douglas.votacaoservice.sessao.repository.entity.SessaoVotacaoEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface SessaoVotacaoMongoRepository extends ReactiveMongoRepository<SessaoVotacaoEntity, String> {
    Mono<SessaoVotacaoEntity> findByUuidPauta(String uuidPauta);
}
