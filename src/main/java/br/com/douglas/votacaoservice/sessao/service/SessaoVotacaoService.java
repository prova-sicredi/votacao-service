package br.com.douglas.votacaoservice.sessao.service;

import br.com.douglas.votacaoservice.sessao.model.SessaoVotacao;
import br.com.douglas.votacaoservice.votacao.model.Voto;
import reactor.core.publisher.Mono;

public interface SessaoVotacaoService {
    Mono<SessaoVotacao> abrirSessao(SessaoVotacao sessaoVotacao);
    Mono<SessaoVotacao> fecharSessao(String uuidPauta);
    Mono<Voto> adicionarVotoASessao(Voto voto);
}
