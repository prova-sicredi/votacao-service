package br.com.douglas.votacaoservice.sessao.service;

import br.com.douglas.votacaoservice.app.exception.GenericExceptionBuilder;
import br.com.douglas.votacaoservice.app.provider.DateTimeProvider;
import br.com.douglas.votacaoservice.sessao.comparator.OpcaoVotoComparator;
import br.com.douglas.votacaoservice.sessao.model.SessaoVotacao;
import br.com.douglas.votacaoservice.sessao.repository.SessaoVotacaoRepository;
import br.com.douglas.votacaoservice.votacao.enumeration.OpcaoVoto;
import br.com.douglas.votacaoservice.votacao.model.Voto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class SessaoVotacaoServiceImpl implements SessaoVotacaoService {

    private static final String MENSAGEM_VOTO_UNICO = "Já consta um voto para o CPF %s.";
    private static final String MENSAGEM_USUARIO_VOTO_UNICO = "Cada usuário só pode votar uma vez.";
    private static final String MENSAGEM_PAUTA_FECHADA = "A sessão da pauta %s já fechou.";
    private static final String MENSAGEM_USUARIO_PAUTA_FECHADA = "Você não pode mais votar nesta pauta. " +
            "Tente votar na próxima.";

    private final SessaoVotacaoRepository sessaoVotacaoRepository;
    private final DateTimeProvider dateTimeProvider;
    private final GenericExceptionBuilder genericExceptionBuilder;

    @Override
    public Mono<SessaoVotacao> abrirSessao(SessaoVotacao sessaoVotacao) {
        return sessaoVotacaoRepository.salvar(sessaoVotacao);
    }

    @Override
    public Mono<SessaoVotacao> fecharSessao(String uuidPauta) {
        return sessaoVotacaoRepository.buscarPorUuidPauta(uuidPauta)
                .flatMap(this::contabilizarVotos)
                .flatMap(sessaoVotacaoRepository::salvar);
    }

    @Override
    public Mono<Voto> adicionarVotoASessao(Voto voto) {
        return sessaoVotacaoRepository.buscarPorUuidPauta(voto.getPauta())
                .filter(this::sessaoAindaEstaAberta)
                .switchIfEmpty(gerarErroSessaoFechada(voto))
                .filter(sessaoVotacao -> usuarioAindaNaoVotou(voto, sessaoVotacao.getVotos()))
                .switchIfEmpty(gerarErroVotoUnico(voto))
                .doOnSuccess(sessaoVotacao -> sessaoVotacao.getVotos().put(voto.getCpf(), voto.getOpcaoVoto()))
                .flatMap(sessaoVotacaoRepository::salvar)
                .then(Mono.just(voto));

    }

    private Mono<SessaoVotacao> gerarErroVotoUnico(Voto voto) {
        return Mono.error(genericExceptionBuilder.buildGenericException(format(MENSAGEM_VOTO_UNICO,voto.getCpf()),
                HttpStatus.PRECONDITION_FAILED, MENSAGEM_USUARIO_VOTO_UNICO));
    }

    private Mono<SessaoVotacao> gerarErroSessaoFechada(Voto voto) {
        return Mono.error(genericExceptionBuilder.buildGenericException(format(MENSAGEM_PAUTA_FECHADA, voto.getPauta()),
                HttpStatus.UNPROCESSABLE_ENTITY, MENSAGEM_USUARIO_PAUTA_FECHADA));
    }

    private boolean usuarioAindaNaoVotou(Voto voto, Map<String, OpcaoVoto> votos) {
        return !votos.containsKey(voto.getCpf());
    }

    private boolean sessaoAindaEstaAberta(SessaoVotacao sessao) {
        return dateTimeProvider.now().isBefore(sessao.getHorarioFechamento());
    }

    private Mono<SessaoVotacao> contabilizarVotos(SessaoVotacao sessao) {
        return Mono.just(OpcaoVoto.gerarContador())
                .doOnSuccess(contador -> contarVotos(sessao, contador))
                .doOnSuccess(contador -> sessao.setResultado(extrairOpcaoMaisVotada(contador)))
                .doOnSuccess(contador -> sessao.setResumoVotacao(extrairResumoDosVotos(contador)))
                .thenReturn(sessao);
    }

    private String extrairOpcaoMaisVotada(Map<OpcaoVoto, Integer> contador) {
        return contador.entrySet().stream()
                .sorted(new OpcaoVotoComparator())
                .filter(this::opcoesSemVoto)
                .findFirst()
                .filter(voto -> naoTemEmpate(contador, voto))
                .map(Map.Entry::getKey)
                .map(OpcaoVoto::name)
                .orElse("Empate");
    }

    private boolean opcoesSemVoto(Map.Entry<OpcaoVoto, Integer> voto) {
        return voto.getValue() > 0;
    }

    private String extrairResumoDosVotos(Map<OpcaoVoto, Integer> contador) {
        return contador.entrySet().stream()
                .sorted(new OpcaoVotoComparator())
                .map(voto -> format("%s: %d", voto.getKey(), voto.getValue()))
                .collect(Collectors.joining(" | "));
    }

    private boolean naoTemEmpate(Map<OpcaoVoto, Integer> contador, Map.Entry<OpcaoVoto, Integer> maisVotado) {
        return contador.values().stream()
                .filter(valor -> valor.equals(maisVotado.getValue()))
                .count() == 1;
    }

    private void contarVotos(SessaoVotacao sessaoVotacao, Map<OpcaoVoto, Integer> contador) {
        sessaoVotacao.getVotos().values().forEach(opcaoVoto -> {
            Integer votosNaOpcao = contador.get(opcaoVoto);
            contador.put(opcaoVoto, votosNaOpcao + 1);
        });
    }
}
