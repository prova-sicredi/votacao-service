package br.com.douglas.votacaoservice.sessao.repository.mapper;

import br.com.douglas.votacaoservice.sessao.model.SessaoVotacao;
import br.com.douglas.votacaoservice.sessao.repository.entity.SessaoVotacaoEntity;
import reactor.core.publisher.Mono;

public class SessaoVotacaoEntityMapper {

    public static Mono<SessaoVotacaoEntity> toSessaoVotacaoEntity(SessaoVotacao sessaoVotacao) {
        return Mono.just(SessaoVotacaoEntity.builder()
                .uuidSessao(sessaoVotacao.getUuidSessao())
                .uuidPauta(sessaoVotacao.getUuidPauta())
                .horarioAbertura(sessaoVotacao.getHorarioAbertura())
                .horarioFechamento(sessaoVotacao.getHorarioFechamento())
                .votos(sessaoVotacao.getVotos())
                .resultado(sessaoVotacao.getResultado())
                .resumoVotacao(sessaoVotacao.getResumoVotacao())
                .build());
    }

    public static Mono<SessaoVotacao> toSessaoVotacao(SessaoVotacaoEntity sessaoVotacaoEntity) {
        return Mono.just(SessaoVotacao.builder()
                .uuidSessao(sessaoVotacaoEntity.getUuidSessao())
                .uuidPauta(sessaoVotacaoEntity.getUuidPauta())
                .horarioAbertura(sessaoVotacaoEntity.getHorarioAbertura())
                .horarioFechamento(sessaoVotacaoEntity.getHorarioFechamento())
                .votos(sessaoVotacaoEntity.getVotos())
                .resultado(sessaoVotacaoEntity.getResultado())
                .resumoVotacao(sessaoVotacaoEntity.getResumoVotacao())
                .build());
    }
}
