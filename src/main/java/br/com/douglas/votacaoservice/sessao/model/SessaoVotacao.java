package br.com.douglas.votacaoservice.sessao.model;

import br.com.douglas.votacaoservice.votacao.enumeration.OpcaoVoto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
public class SessaoVotacao {
    private String uuidSessao;
    private String uuidPauta;
    private LocalDateTime horarioAbertura;
    private LocalDateTime horarioFechamento;
    private Map<String, OpcaoVoto> votos;
    private String resultado;
    private String resumoVotacao;
}
