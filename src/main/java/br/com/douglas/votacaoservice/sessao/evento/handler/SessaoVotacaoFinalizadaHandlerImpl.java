package br.com.douglas.votacaoservice.sessao.evento.handler;

import br.com.douglas.votacaoservice.sessao.evento.model.ResultadoSessaoVotacao;
import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoFinalizada;
import br.com.douglas.votacaoservice.sessao.evento.produtor.PublicadorResultadoSessaoVotacao;
import br.com.douglas.votacaoservice.sessao.mapper.SessaoVotacaoMapper;
import br.com.douglas.votacaoservice.sessao.service.SessaoVotacaoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class SessaoVotacaoFinalizadaHandlerImpl implements SessaoVotacaoFinalizadaHandler {

    private final SessaoVotacaoService sessaoVotacaoService;
    private final SessaoVotacaoMapper sessaoVotacaoMapper;
    private final PublicadorResultadoSessaoVotacao publicadorResultadoSessaoVotacao;

    @Override
    public Mono<ResultadoSessaoVotacao> finalizarSessao(SessaoVotacaoFinalizada sessaoVotacaoFinalizada) {
        return sessaoVotacaoService.fecharSessao(sessaoVotacaoFinalizada.getUuidPauta())
                .flatMap(sessaoVotacaoMapper::toResultadoSessaoVotacao)
                .doOnSuccess(publicadorResultadoSessaoVotacao::publicar);
    }
}
