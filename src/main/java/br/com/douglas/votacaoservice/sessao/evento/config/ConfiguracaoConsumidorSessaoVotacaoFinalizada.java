package br.com.douglas.votacaoservice.sessao.evento.config;

import br.com.douglas.votacaoservice.app.config.AbstractKafkaConfiguration;
import br.com.douglas.votacaoservice.app.config.PropriedadesConsumidor;
import br.com.douglas.votacaoservice.app.config.PropriedadesKafka;
import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoFinalizada;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.retry.RetryContext;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.SuccessCallback;

import static br.com.douglas.votacaoservice.votacao.constantes.ChavesPropriedadesEventosKafka.EVENTO_SESSAO_VOTACAO_FINALIZADA_CONSUMER_FACTORY;
import static br.com.douglas.votacaoservice.votacao.constantes.ChavesPropriedadesEventosKafka.EVENTO_SESSAO_VOTACAO_FINALIZADA_KAFKA_LISTENER_CONTAINER_FACTORY;
import static java.util.Optional.empty;
import static org.springframework.kafka.listener.adapter.RetryingMessageListenerAdapter.CONTEXT_RECORD;

@Slf4j
@Component
public class ConfiguracaoConsumidorSessaoVotacaoFinalizada extends AbstractKafkaConfiguration {

    private final KafkaTemplate<String, SessaoVotacaoFinalizada> kafkaTemplate;

    public ConfiguracaoConsumidorSessaoVotacaoFinalizada(PropriedadesKafka propriedadesKafka,
                                                         KafkaTemplate<String, SessaoVotacaoFinalizada> kafkaTemplate) {
        super(propriedadesKafka);
        this.kafkaTemplate = kafkaTemplate;
    }

    @Bean(EVENTO_SESSAO_VOTACAO_FINALIZADA_KAFKA_LISTENER_CONTAINER_FACTORY)
    public ConcurrentKafkaListenerContainerFactory<String, SessaoVotacaoFinalizada> eventoSessaoVotacaoFinalizadaListener(
            ConsumerFactory<String, SessaoVotacaoFinalizada> consumerFactory) {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, SessaoVotacaoFinalizada>();
        factory.setConsumerFactory(consumerFactory);
        factory.setRecoveryCallback(this::recoveryCallback);
        return factory;
    }

    @Bean(EVENTO_SESSAO_VOTACAO_FINALIZADA_CONSUMER_FACTORY)
    public ConsumerFactory<String, SessaoVotacaoFinalizada> eventoSessaoVotacaoFinalizadaConsumerFactory() {
        return new DefaultKafkaConsumerFactory<>(
                configuracoesConsumidor(),
                new StringDeserializer(),
                new JsonDeserializer<>(SessaoVotacaoFinalizada.class)
        );
    }

    private Object recoveryCallback(RetryContext context) {
        var record = (ConsumerRecord<String, SessaoVotacaoFinalizada>) context.getAttribute(CONTEXT_RECORD);
        SessaoVotacaoFinalizada evento = record.value();
        PropriedadesConsumidor proriedadesConsumidor = propriedadesKafka.getProriedadesConsumidor();
        kafkaTemplate.send(proriedadesConsumidor.getTopicoSessaoFinalizadaError(), record.key(), evento)
                .addCallback(
                        callbackEnvioComSucesso(evento),
                        callbackErroEnvio(evento)
                );
        return empty();
    }

    private SuccessCallback<SendResult<String, SessaoVotacaoFinalizada>> callbackEnvioComSucesso(SessaoVotacaoFinalizada evento) {
        return result -> log.warn("Enviado SessaoVotacaoFinalizada para DLQ: {}.", evento);
    }

    private FailureCallback callbackErroEnvio(SessaoVotacaoFinalizada evento) {
        return error -> log.error("Erro ao enviar SessaoVotacaoFinalizada para DLQ: " + evento, error);
    }
}
