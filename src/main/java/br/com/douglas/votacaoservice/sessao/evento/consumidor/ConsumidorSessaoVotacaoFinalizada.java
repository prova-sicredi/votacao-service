package br.com.douglas.votacaoservice.sessao.evento.consumidor;

import br.com.douglas.votacaoservice.sessao.evento.handler.SessaoVotacaoFinalizadaHandler;
import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoFinalizada;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import static br.com.douglas.votacaoservice.app.constants.ChavesConfiguracoes.APPLICATION_ID_CONFIG;
import static br.com.douglas.votacaoservice.app.constants.ChavesConfiguracoes.TOPICO_SESSAO_FINALIZADA;
import static br.com.douglas.votacaoservice.votacao.constantes.ChavesPropriedadesEventosKafka.EVENTO_SESSAO_VOTACAO_FINALIZADA_KAFKA_LISTENER_CONTAINER_FACTORY;
import static java.lang.String.format;

@Component
@Slf4j
@RequiredArgsConstructor
public class ConsumidorSessaoVotacaoFinalizada {

    public static final String LOG_VOTACAO_FINALIZADA = "Finalizada apuração da votação para a pauta %s: Resultado - %s";
    private final SessaoVotacaoFinalizadaHandler sessaoVotacaoFinalizadaHandler;

    @KafkaListener(
            topics = TOPICO_SESSAO_FINALIZADA,
            groupId = APPLICATION_ID_CONFIG,
            containerFactory = EVENTO_SESSAO_VOTACAO_FINALIZADA_KAFKA_LISTENER_CONTAINER_FACTORY
    )
    public void listen(@Payload SessaoVotacaoFinalizada sessaoVotacaoFinalizada) {
        sessaoVotacaoFinalizadaHandler.finalizarSessao(sessaoVotacaoFinalizada)
                .doOnSuccess(resultadoVotacao -> log.info(format(LOG_VOTACAO_FINALIZADA,
                        resultadoVotacao.getUuidPauta(), resultadoVotacao.getResultado())))
                .subscribe();
    }

}
