package br.com.douglas.votacaoservice.sessao.repository;

import br.com.douglas.votacaoservice.app.exception.GenericExceptionBuilder;
import br.com.douglas.votacaoservice.sessao.model.SessaoVotacao;
import br.com.douglas.votacaoservice.sessao.repository.mapper.SessaoVotacaoEntityMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
public class SessaoVotacaoRepositoryImpl implements SessaoVotacaoRepository {

    private static final String ERRO_SALVAR = "Erro ao salvar a sessão para a pauta %s";
    private static final String ERRO_USUARIO_SALVAR = "Tente novamente mais tarde";
    private static final String ERRO_USUARIO_BUSCAR = "Não existe uma sessão para a pauta %s";
    private static final String ERRO_BUSCAR = "Sessão de votação não encontrada para a pauta informada.";

    private final SessaoVotacaoMongoRepository sessaoVotacaoMongoRepository;
    private final GenericExceptionBuilder genericExceptionBuilder;

    @Override
    public Mono<SessaoVotacao> salvar(SessaoVotacao sessaoVotacao) {
        return SessaoVotacaoEntityMapper.toSessaoVotacaoEntity(sessaoVotacao)
                .flatMap(sessaoVotacaoMongoRepository::save)
                .switchIfEmpty(Mono.error(genericExceptionBuilder.buildGenericException(
                        format(ERRO_SALVAR, sessaoVotacao.getUuidPauta()),
                        HttpStatus.INTERNAL_SERVER_ERROR, ERRO_USUARIO_SALVAR)))
                .flatMap(SessaoVotacaoEntityMapper::toSessaoVotacao);
    }

    @Override
    public Mono<SessaoVotacao> buscarPorUuidPauta(String uuidPauta) {
        return sessaoVotacaoMongoRepository.findByUuidPauta(uuidPauta)
                .switchIfEmpty(Mono.error(genericExceptionBuilder.buildGenericException(ERRO_BUSCAR,
                        HttpStatus.NOT_FOUND, format(ERRO_USUARIO_BUSCAR, uuidPauta))))
                .flatMap(SessaoVotacaoEntityMapper::toSessaoVotacao);
    }
}
