package br.com.douglas.votacaoservice.sessao.evento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SessaoVotacaoFinalizada {
    private String uuidPauta;
}
