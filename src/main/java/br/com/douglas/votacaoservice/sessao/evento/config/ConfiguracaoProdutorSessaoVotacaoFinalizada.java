package br.com.douglas.votacaoservice.sessao.evento.config;

import br.com.douglas.votacaoservice.app.config.AbstractKafkaConfiguration;
import br.com.douglas.votacaoservice.app.config.PropriedadesKafka;
import br.com.douglas.votacaoservice.sessao.evento.model.SessaoVotacaoFinalizada;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class ConfiguracaoProdutorSessaoVotacaoFinalizada extends AbstractKafkaConfiguration {

    public ConfiguracaoProdutorSessaoVotacaoFinalizada(PropriedadesKafka propriedadesKafka) {
        super(propriedadesKafka);
    }

    @Bean
    public KafkaTemplate<String, SessaoVotacaoFinalizada> kafkaTemplateSessaoFinalizada() {
        return new KafkaTemplate<>(sessaoFinalizadaKafkaProducerFactory());
    }

    public ProducerFactory<String, SessaoVotacaoFinalizada> sessaoFinalizadaKafkaProducerFactory() {
        return new DefaultKafkaProducerFactory<>(configuracoesProdutor());
    }
}
